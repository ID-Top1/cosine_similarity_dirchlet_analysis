# Cosine Similarity Dirchlet Analysis

Code using dirichlet-multinomial distribution as a null model for assessing significance of cosine similarities of mutation spectra of differing dimensions.

## Requirements

The following R libraries are required:

* extraDistr
* magrittr
* ggplot2
* cowplot
* lsa
* R.devices

To install these libraries from within R:

    > install.packages(c("extraDistr", "magrittr", "ggplot2", "cowplot", "lsa", "R.devices"))

## Usage:

Clone this repository and run the dirichlet_cosine_pvals_submission.R and dirichlet_nulldistribution_plots.R R scripts to create plots for the null distribution of cosine similarities for random vectors and to calculate p-values for cosine similarities of mutation signatures for yeast/HeLa reporters from the paper.

For example:

~~~
$ git clone https://git.ecdf.ed.ac.uk/Deletions_paper/cosine_similarity_dirchlet_analysis.git
$ cd cosine_similarity_dirchlet_analysis
$ Rscript dirichlet_nulldistribution_plots.R  
$ Rscript dirichlet_cosine_pvals_submission.R
~~~

A new "output" folder will be created containing plot and CSV outputs.

Outputs reported in the paper were generated using R version 4.1.0 on a linux system. Due to random number generation, using different R versions may result in slightly altered values.

## Author

Michael Nicholson, University of Edinburgh
